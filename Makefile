-include common.mk

.PHONY=help package clean
.DEFAULT=help

OPTIONAL_GIT_DEPS=

SRC_DIR=src
OUT_DIR=out
BUILD_DIR=out/build
PACKAGES_DIR=$(OUT_DIR)/work/packages
STANDALONE_BUILD=$(OUT_DIR)/work/standalone
PARTITIONED_DIR=$(OUT_DIR)/release/partitioned
STANDALONE_DIR=$(OUT_DIR)/release/splunkbase
OPTIONAL_DEPENDENCY_DIR=$(OUT_DIR)/work/optional_dependencies
TEST_RESULTS=test-reports

MAIN_APP ?= $(shell ls -1 $(APPS_DIR))
MAIN_APP_DESC ?= Add on for Splunk

#COPYRIGHT_CMD ?= copyright-header
COPYRIGHT_DOCKER_IMAGE = splseckit/copyright-header:latest
COPYRIGHT_CMD ?= docker run --rm --volume `pwd`:/usr/src/ $(COPYRIGHT_DOCKER_IMAGE)
COPYRIGHT_LICENSE ?= ASL2
COPYRIGHT_HOLDER ?= Splunk Inc.
COPYRIGHT_YEAR ?= 2018
COPYRIGHT_SOFTWARE ?= $(MAIN_APP)
COPYRIGHT_SOFTWARE_DESCRIPTION ?= $(MAIN_APP_DESC)
COPYRIGHT_OUTPUT_DIR ?= .
COPYRIGHT_WORD_WRAP ?= 100
COPYRIGHT_PATHS ?= $(APPS_DIR)


VERSION=$(shell gitversion /showvariable MajorMinorPatch)
BUILD_NUMBER?=0000
COMMIT_ID ?=$(shell git rev-parse --short HEAD)
BRANCH?=$(shell git branch | grep \* | cut -d ' ' -f2)

APPS_ALL = $(shell find $(SRC_DIR) -maxdepth 1 -type d -mindepth 1 | cut -d'/' -f2-)
STANDALONE_DEP_DIR=$(STANDALONE_BUILD)/$(MAIN_APP)/appserver/addons

PACKAGE_SLUG=A$(COMMIT_ID)
ifneq (,$(findstring master, $(BRANCH) ))
	PACKAGE_SLUG=R$(COMMIT_ID)
endif
ifneq (,$(findstring release, $(BRANCH) ))
	PACKAGE_SLUG=B$(COMMIT_ID)
endif


help: ## Show this help message.
	@echo 'usage: make [target] ...'
	@echo
	@echo 'targets:'
	@egrep '^(.+)\:\ ##\ (.+)' $(MAKEFILE_LIST) | column -t -c 2 -s ':#' | sed 's/^/  /'

prebuild:
	@mkdir -p $(OUT_DIR)
	@mkdir -p $(PACKAGES_DIR)
	@mkdir -p $(PARTITIONED_DIR)

clean: ## Remove artifacts
	@rm -rf $(OUT_DIR)

$(PACKAGES_DIR):
	@mkdir -p $(PACKAGES_DIR)

$(PARTITIONED_DIR):
	@mkdir -p $(PARTITIONED_DIR)

$(STANDALONE_BUILD):
	@mkdir -p $(STANDALONE_BUILD)

$(STANDALONE_DIR):
	@mkdir -p $(STANDALONE_DIR)

add-copyright: ## Add copyright notice header to supported file types
add-copyright:
	$(COPYRIGHT_CMD) \
	  --license $(COPYRIGHT_LICENSE)  \
	  --add-path $(COPYRIGHT_PATHS) \
	  --guess-extension \
	  --copyright-holder '$(COPYRIGHT_HOLDER)' \
	  --copyright-software '$(COPYRIGHT_SOFTWARE)' \
	  --copyright-software-description '$(COPYRIGHT_SOFTWARE_DESCRIPTION)' \
	  --copyright-year $(COPYRIGHT_YEAR) \
	  --word-wrap $(COPYRIGHT_WORD_WRAP) \
	  --output-dir $(COPYRIGHT_OUTPUT_DIR)

bump: ## Set the file versions for check into repo
bump: clean prebuild add-copyright
	@echo bumping file versions to $(VERSION)
	@echo $(APPS_ALL)
	@for i in $(APPS_ALL); do \
		crudini --set $(SRC_DIR)/$$i/default/app.conf launcher version $(VERSION)$(PACKAGE_SLUG);\
		crudini --set $(SRC_DIR)/$$i/default/app.conf install build $(BUILD_NUMBER);\
		slim generate-manifest --update -o $(OUT_DIR)/app.manifest $(SRC_DIR)/$$i ;\
		cp $(OUT_DIR)/app.manifest $(SRC_DIR)/$$i/app.manifest ;\
	done


package: ## Package each app
package: clean prebuild
	@mkdir -p $(OUT_DIR)
	@mkdir -p $(BUILD_DIR)
	@mkdir -p $(PACKAGES_DIR)
	@mkdir -p $(PARTITIONED_DIR)

	@for i in $(MAIN_APP) $(DEPLOYMENT_APPS); do \
		cp -R src/$$i $(BUILD_DIR) ;\
		crudini --set $(BUILD_DIR)/$$i/default/app.conf launcher version $(VERSION)$(PACKAGE_SLUG);\
		crudini --set $(BUILD_DIR)/$$i/default/app.conf install build $(BUILD_NUMBER);\
		slim generate-manifest --update -o $(OUT_DIR)/app.manifest $(BUILD_DIR)/$$i ;\
		cp $(OUT_DIR)/app.manifest $(BUILD_DIR)/$$i/app.manifest ;\
	done

	for i in $(MAIN_APP) $(DEPLOYMENT_APPS); do find $(BUILD_DIR)/$$i  -type d -exec bash -c "chmod o-w,g-w,a+X '{}'" \; ; done
	for i in $(MAIN_APP) $(DEPLOYMENT_APPS); do find $(BUILD_DIR)/$$i  -type f -exec bash -c "chmod o-w,g-w,a-x '{}'" \; ; done
	for i in $(MAIN_APP) $(DEPLOYMENT_APPS); do if [ ! -d $(BUILD_DIR)/$$i/bin ]; then break; fi ; chmod u+x,g+x $(BUILD_DIR)/$$i/bin/* ; done
	for i in $(MAIN_APP) $(DEPLOYMENT_APPS); do slim package -o $(STANDALONE_DIR) $(BUILD_DIR)/$$i ; done

	slim partition -o $(PARTITIONED_DIR) $(STANDALONE_DIR)/$(MAIN_APP)-$(VERSION)-$(PACKAGE_SLUG).tar.gz

package_test: ## Package Test
package_test: package
	@mkdir -p test-reports
	splunk-appinspect inspect $(STANDALONE_DIR)/$(MAIN_APP)-$(VERSION)-$(PACKAGE_SLUG).tar.gz --data-format junitxml --output-file test-reports/precert-cloud.xml --mode precert --included-tags cloud
	splunk-appinspect inspect $(STANDALONE_DIR)/$(MAIN_APP)-$(VERSION)-$(PACKAGE_SLUG).tar.gz --data-format junitxml --output-file test-reports/precert.xml --mode precert
