#Splunk Add on Development Community Guide
## Starting a project plan ahead

- Create and App "id"
  - Is this a "TA" ()
    - Use the format TA-vendor_product for example "TA-apache_kafka"
      - "TA" should always be first and upper case
      - "vendor" is the organization name optional when the organization and product are the same i.e mariadb the redundant value can be omitted
      - "product"
- Select a License
    - Apache 2.0 requires derivative works to license under the same. While a third party could also offer an additional license a derivative would be "dual" licensed. Additional steps are required in the release process however this seems to be the most value leading choice
    - Splunk Software license. Precluded modification and derivative works this is problematic in the ecosystem as rights to extend providing additional value are precluded in the plain language while encouraged in marketing.

## Start development

### Prepare local dev system

- Install docker
- Prepare a python virtual env "splunkdev"
- Install requirements
- Install git client
- Install git-flow-avh (note this is not same as the original git flow tool)
  ``brew install git-flow-avh``
- Setup a working directory for all addons
``mkdir -p ~/splunkaddons``

## Start the project locally

- create the project directory not all lower case
  ``mkdir ta-splunk_addon_dev_demo``
- cd into the working directory
  ``cd ta-splunk_addon_dev_demo``
- initialize the git repo with git flow, take all default options
  ``git flow init``
- Create an initial structure based on the example/ in this project

  ```bash
  ├── docs
  │   ├── docs #Copied to the built project root directory
  │   │   ├── **/*
  │   ├── README.md
  │   ├── LICENSE* # License File
  │   ├── SUPPORT.MD #Support Policy Statement
  │   ├── favicon.ico
  │   └── templates/**/*
  ├── src
  │   ├── TA-vendor_product #Tech Add on content
  │   │   ├── app.manifest
  │   │   ├── default
  │   │   │   ├── app.conf
  │   │   ├── metadata
  │   │   │   ├── default.meta
  │   │   ├── **/*
  ├── README.md # Project Readme this is not included in the addon
  ├── common.mk # Optional build configuration file
  └── .gitignore
  ```

  ## Setup the repository on Bitbucket

  NOTE the project admin will create the project the steps that follow are for transparency

  - Browse to and Login https://bitbucket.org/account/user/SPLServices/projects/FAC
  - Click the "+" Icon in the left nav bar then click repository
  - Set the project name as "Field Addon Content"
  - Name the repository using the lower case name of the working folder i.e. ta-splunk_addon_dev_demo
  - Remove "Private" check from access level
  - Ensure version control is "git"
  - View Advanced settings
  - Enable Issue tracking
  - Select Language as "python"; do this even if no python is used in this add on
  - Click Create Repository

## Link the local repository to the remote

  - ``git remote add origin git@bitbucket.org:SPLServices/ta-devdemo_addon_splunk.git``
  - ``git tag v0.1.0``
  - ``git push --all``

## Start work

  - ``git flow feature start <name of feature>`` For example setting up a project from template the feature coul be called "NewProject"
  - Use typical git add git commit
  - When feature is complete use the command ``git flow finish`` to merge to develop
