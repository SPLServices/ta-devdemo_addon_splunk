---
Description: Splunk Technical Add on Development Demo project

splunk_id: TA-devdemo_addon_splunk
---

# DevDemo Technology Add on for splunk

![Hex.pm](https://img.shields.io/hexpm/l/plug.svg) ![Bitbucket issues](https://img.shields.io/bitbucket/issues/SPLServices/ta-devdemo_addon_splunk.svg)
![SemVer Compatibility](https://img.shields.io/badge/SEMVER-2.0-green.svg)
